package com.atlassian.stash.plugin.hook.data;

import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.rest.data.RestMapEntity;

public class RestRefChange extends RestMapEntity {

	private static final long serialVersionUID = 1L;

	public RestRefChange(RefChange refChange) {
		put("refId", refChange.getRefId());
		put("fromHash", refChange.getFromHash());
		put("toHash", refChange.getToHash());
		put("type", refChange.getType().name());
	}

}
