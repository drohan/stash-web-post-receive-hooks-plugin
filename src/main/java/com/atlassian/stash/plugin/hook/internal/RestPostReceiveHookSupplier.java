package com.atlassian.stash.plugin.hook.internal;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nullable;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.content.DetailedChangeset;
import com.atlassian.stash.content.DetailedChangesetsRequest;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.plugin.hook.data.RestPostReceiveHook;
import com.atlassian.stash.plugin.hook.data.RestRefChange;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.data.RestDetailedChangeset;
import com.atlassian.stash.rest.data.RestPage;
import com.atlassian.stash.rest.data.RestRepository;
import com.atlassian.stash.rest.data.RestStashUser;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageUtils;
import com.atlassian.stash.util.UncheckedOperation;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Transforms hook {@link RefChange}-s to appropriate request entity.
 */
public class RestPostReceiveHookSupplier extends LazyReference<RestPostReceiveHook> {

	private static final Function<Changeset, String> PLUCK_CHANGESET_ID = new Function<Changeset, String>() {

		@Override
		public String apply(@Nullable Changeset input) {
			return input.getId();
		}
	};
	private static final Function<RefChange, String> PLUCK_FROM_HASH = new Function<RefChange, String>() {

		@Override
		public String apply(@Nullable RefChange input) {
			return input.getFromHash();
		}

	};
	private static final Function<RefChange, String> PLUCK_TO_HASH = new Function<RefChange, String>() {

		@Override
		public String apply(@Nullable RefChange input) {
			return input.getToHash();
		}

	};
	private static final Function<RefChange, RestRefChange> TO_REST_REF_CHANGE = new Function<RefChange, RestRefChange>() {

		@Override
		public RestRefChange apply(@Nullable RefChange input) {
			return new RestRefChange(input);
		}

	};
	private final Function<DetailedChangeset, RestDetailedChangeset> TO_REST_CHANGESET = new Function<DetailedChangeset, RestDetailedChangeset>() {

		@Override
		public RestDetailedChangeset apply(DetailedChangeset input) {
			return new RestDetailedChangeset(input, navBuilder, false);
		}

	};

	private final SecurityService securityService;
	private final HistoryService historyService;
	private final NavBuilder navBuilder;
	private final int changesetsLimit;

	private final Repository repository;
	private final Collection<RefChange> refChanges;
	private final StashUser currentUser;
	private final int changesLimit;

	public RestPostReceiveHookSupplier(SecurityService securityService, HistoryService historyService, NavBuilder navBuilder,
			int changesetsLimit, int changesLimit, Repository repository, Collection<RefChange> refChanges,
			StashUser currentUser) {
		this.securityService = securityService;
		this.historyService = historyService;
		this.navBuilder = navBuilder;
		this.changesetsLimit = changesetsLimit;
		this.changesLimit = changesLimit;
		this.repository = repository;
		this.refChanges = refChanges;
		this.currentUser = currentUser;
	}

	@Override
	public RestPostReceiveHook create() {
		return securityService.doWithPermission("Async PostReceive Web Hook", Permission.REPO_READ,
				new UncheckedOperation<RestPostReceiveHook>() {

					@Override
					public RestPostReceiveHook perform() throws RuntimeException {
						Iterable<RestRefChange> restRefChanges = Iterables.transform(refChanges, TO_REST_REF_CHANGE);

						final ChangesetsBetweenRequest changesetsRequest = new ChangesetsBetweenRequest.Builder(repository) //
								.include(Iterables.transform(refChanges, PLUCK_TO_HASH)) //
								.exclude(Iterables.transform(refChanges, PLUCK_FROM_HASH)) //
								.build();

						Page<Changeset> changesets = historyService.getChangesetsBetween(changesetsRequest,
								PageUtils.newRequest(0, changesetsLimit));

						Iterable<RestDetailedChangeset> restDetailedChangesets;
                        if (changesets.getSize() == 0) {
							restDetailedChangesets = Collections.<RestDetailedChangeset> emptyList();

						} else {
							DetailedChangesetsRequest detailedChangesetsRequest = new DetailedChangesetsRequest.Builder(repository)
									.changesetIds(Iterables.transform(changesets.getValues(), PLUCK_CHANGESET_ID))
									.maxChangesPerCommit(changesLimit).build();
							Page<DetailedChangeset> detailedChangesets = historyService.getDetailedChangesets(detailedChangesetsRequest,
									PageUtils.newRequest(0, changesetsLimit));
							// transforms changesets to appropriate REST view
							restDetailedChangesets = Iterables.transform(detailedChangesets.getValues(), TO_REST_CHANGESET);

						}

						RestPage<RestDetailedChangeset> changesetsPage = new RestPage<RestDetailedChangeset>(changesets.getStart(),
								changesets.getLimit(), changesets.getSize(), changesets.getIsLastPage(), restDetailedChangesets, changesets
										.getNextPageRequest(), null);

						return new RestPostReceiveHook(new RestRepository(repository), restRefChanges, changesetsPage,
							currentUser != null ? new RestStashUser(currentUser) : null);
					}

				});
	}

}
